package com.example.currencyexchange.data.model

data class CurrencyModel(
    var base: String = "",
    var date: String = "",
    var rates: Map<String, Double> = HashMap(),
    var success: Boolean = false,
    var timestamp: Int = 0,
)