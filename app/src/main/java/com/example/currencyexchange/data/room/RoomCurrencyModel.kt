package com.example.currencyexchange.data.room

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "currency_model")
data class RoomCurrencyModel(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    var base: String,
    var date: String,
    var rates: String,
    var success: Boolean,
    var timestamp: Int,
)