package com.example.currencyexchange.data.model


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CurrencyConvertResponse(
    @Json(name = "date")
    val date: String,
    @Json(name = "info")
    val info: Info,
    @Json(name = "query")
    val query: Query,
    @Json(name = "result")
    val result: Double,
    @Json(name = "success")
    val success: Boolean
)

@JsonClass(generateAdapter = true)
data class Info(
    @Json(name = "rate")
    val rate: Double,
    @Json(name = "timestamp")
    val timestamp: Int
)

@JsonClass(generateAdapter = true)
data class Query(
    @Json(name = "amount")
    val amount: Double,
    @Json(name = "from")
    val from: String,
    @Json(name = "to")
    val to: String
)