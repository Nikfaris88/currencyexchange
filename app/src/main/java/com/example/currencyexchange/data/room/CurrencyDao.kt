package com.example.currencyexchange.data.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface CurrencyDao{

    @Query("SELECT * FROM currency_model WHERE base = :base")
    fun getExchangeRateByBaseCurrency(base: String): RoomCurrencyModel?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertNewExchangeRate(currencyModel: RoomCurrencyModel)
}