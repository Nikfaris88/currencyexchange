package com.example.currencyexchange.data.api

import com.example.currencyexchange.data.model.CurrencyConvertResponse
import com.example.currencyexchange.data.model.CurrencyModel
import com.example.currencyexchange.utils.Constants.Companion.CONVERT_ENDPOINT
import com.example.currencyexchange.utils.Constants.Companion.CURRENCY_ENDPOINT
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET(CURRENCY_ENDPOINT)
    suspend fun getExchangeRates(
        @Query("access_key") accessKey: String,
        @Query("base") base: String? = null,
        @Query("symbols") symbols: String? = null,
    ): CurrencyModel

    @GET(CONVERT_ENDPOINT)
    suspend fun convertRates(
        @Query("access_key") accessKey: String,
        @Query("from") from: String? = null,
        @Query("to") to: String? = null,
        @Query("amount") amount:Double
    ): CurrencyConvertResponse
}
