package com.example.currencyexchange.data.contract

import com.example.currencyexchange.data.model.CurrencyModel
import com.example.currencyexchange.data.room.RoomCurrencyModel

interface CurrencyContract {
    interface Model {

        interface OnFinishedListener {
            fun onFinished(currencyModel: CurrencyModel)
            fun onFailure(t: Throwable)
            fun onFinished(roomCurrencyModel: RoomCurrencyModel)
        }

        fun getExchangeRates(base:String, onFinishedListener: OnFinishedListener)

        fun convertRates(from:String, to: String, amount: Double, onFinishedListener: OnFinishedListener)
    }

    interface View {
        fun showProgress()
        fun hideProgress()
        fun setData(currencyModel: CurrencyModel)
        fun setLocalData(roomCurrencyModel: RoomCurrencyModel)
        fun onResponseFailure(t: Throwable)
    }

    interface Presenter {
        fun onDestroy()
        fun requestData(base: String)
        fun convertCurrency(from: String, to: String, amount: Double)
    }
}