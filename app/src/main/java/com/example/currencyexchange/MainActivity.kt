package com.example.currencyexchange

import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import com.example.currencyexchange.data.contract.CurrencyContract
import com.example.currencyexchange.data.model.CurrencyModel
import com.example.currencyexchange.data.room.CurrencyDatabase
import com.example.currencyexchange.data.room.RoomCurrencyModel
import com.example.currencyexchange.presenter.CurrencyPresenter
import com.example.currencyexchange.ui.CurrencyChart
import com.example.currencyexchange.ui.theme.CurrencyExchangeTheme
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import com.github.mikephil.charting.utils.Utils
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainActivity : ComponentActivity(), CurrencyContract.View {

    private lateinit var currencyPresenter: CurrencyPresenter
    private var currencyData: CurrencyModel? = null

    private var isLoading by mutableStateOf(false)

//    private val labels = listOf(
//        "Sept 13",
//        "Sept 14",
//        "Sept 15",
//        "Sept 16",
//        "Sept 17"
//    )
//    private val entries = listOf(
//        Entry(1.34F, 2.0F),
//        Entry(2.44F, 3.0F),
//        Entry(3.54F, 4.5F),
//        Entry(4.64F, 5.0F),
//        Entry(5.55F, 6.5F)
//    )

    private val dataList = listOf<PopCount>(
        PopCount(2023, 1.5),
        PopCount(2024, 2.1),
        PopCount(2025, 3.5),
        PopCount(2026, 4.5),
        PopCount(2027, 5.5),
        PopCount(2028, 2.5),
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val currencyDatabase = CurrencyDatabase.getDatabase(this.application)
        val currencyDao = currencyDatabase.currencyDao()

        currencyPresenter = CurrencyPresenter(this, application, currencyDao = currencyDao)
        CoroutineScope(Dispatchers.IO).launch {
            currencyPresenter.requestData(base = "EUR")
        }

        //UI implementation
        setContent {
            CurrencyExchangeTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = Color.White
                ) {
                    if (isLoading) {
                        Box(
                            modifier = Modifier.padding(24.dp),
                            Alignment.Center

                        ) {
                            CircularProgressIndicator()
                        }
                    } else {

                        Column(
                            modifier = Modifier
                                .padding(16.dp)
                        ) {
                            Box(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .fillMaxHeight(0.5F)
                            ) {
                                Title(
                                    title = "Currency Exchange",
                                    currencyData = currencyData,
                                    currencyPresenter = currencyPresenter
                                )
                            }

                            Spacer(modifier = Modifier.height(16.dp))

                            Box(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .fillMaxHeight(1F)
                            ) {
                                //Sample data because can't get it from the API
                                CurrencyChartView(populationData = dataList)
//                                LineChartComposable(
//                                    labels = labels,
//                                    entries = entries,
//                                )
                            }
                        }

                    }
                }
            }
        }
    }

    override fun showProgress() {
        isLoading = true
    }

    override fun hideProgress() {
        isLoading = false
    }

    override fun setData(currencyModel: CurrencyModel) {
        this.currencyData = currencyModel
    }

    override fun setLocalData(roomCurrencyModel: RoomCurrencyModel) {
        val currencyModel = CurrencyModel(
            base = roomCurrencyModel.base,
            date = roomCurrencyModel.date,
            timestamp = roomCurrencyModel.timestamp,
            success = roomCurrencyModel.success,
            rates = Gson().fromJson(
                roomCurrencyModel.rates,
                object : TypeToken<HashMap<String, Double>>() {}.type
            )
        )
        this.currencyData = currencyModel
    }

    override fun onResponseFailure(t: Throwable) {
        isLoading = false
    }
}

@Composable
fun Title(
    title: String,
    currencyData: CurrencyModel?,
    modifier: Modifier = Modifier,
    currencyPresenter: CurrencyPresenter
) {
    Column(
        modifier = modifier
            .fillMaxWidth()
            .padding(24.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = title,
            style = TextStyle(fontSize = 16.sp, fontWeight = FontWeight.Bold)
        )
        Spacer(modifier = modifier.padding(16.dp))

        if (currencyData != null) {
            CurrencyConverter(
                currencyModel = currencyData,
                currencyPresenter = currencyPresenter
            )
        }
    }
}

@Composable
fun CurrencyConverter(currencyModel: CurrencyModel?, currencyPresenter: CurrencyPresenter) {
    val context = LocalContext.current
    var expandedBase by remember { mutableStateOf(false) }
    var baseAmount by remember { mutableStateOf("1.0") }

    var expandedSelected by remember { mutableStateOf(false) }
    val baseCurrency by remember { mutableStateOf("EUR") }
    var selectedCurrency by remember { mutableStateOf("MYR") }

    val rates = currencyModel?.rates
    val baseCurrencyCode: MutableSet<String> = HashSet()
    for (code in rates?.keys!!) {
        baseCurrencyCode.add(code)
    }

    val selectedCurrencyCode: MutableSet<String> = HashSet()
    for (code in rates.keys) {
        selectedCurrencyCode.add(code)
    }

    var convertedAmount by remember { mutableStateOf(rates[baseCurrency]?.toString()) }

    LaunchedEffect(baseCurrency, baseAmount, selectedCurrency) {
        val rate = rates[selectedCurrency]
        val amount = baseAmount.toDoubleOrNull()

        convertedAmount = if (rate != null && amount != null) {
            (rate * amount).toString()
        } else {
            "0"
        }
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Top
    ) {

        Text(
            text = "Base Currency",
            modifier = Modifier.fillMaxWidth(),
            textAlign = TextAlign.Start
        )

        Row {

            Box(
                modifier = Modifier
                    .clickable {
                        expandedBase = true
                    }
                    .border(0.5.dp, Color.Gray, shape = RoundedCornerShape(4.dp))
                    .background(Color.White)
                    .padding(horizontal = 8.dp, vertical = 8.dp)
            ) {
                Text(
                    text = baseCurrency,
                    modifier = Modifier
                        .width(90.dp)
                        .height(20.dp),
                    textAlign = TextAlign.Center
                )
                Icon(
                    imageVector = Icons.Default.ArrowDropDown,
                    contentDescription = null,
                    modifier = Modifier.align(Alignment.CenterEnd)
                )
            }

            DropdownMenu(
                expanded = false,
                onDismissRequest = {
                    expandedBase = false
                },
                modifier = Modifier.width(80.dp)
            ) {
                baseCurrencyCode.sorted().forEach { item ->
                    DropdownMenuItem(
                        onClick = {
//                                    baseCurrency = item
                            expandedBase = false
                        },
                        modifier = Modifier
                            .width(80.dp)
                    ) {
                        Text(text = item)
                    }
                }
            }
            Spacer(modifier = Modifier.width(5.dp))
            Box(
                modifier = Modifier
                    .border(1.dp, Color.Gray, shape = RoundedCornerShape(4.dp))
                    .background(Color.White)
                    .padding(horizontal = 8.dp, vertical = 9.dp)
            ) {
                BasicTextField(
                    value = baseAmount,
                    onValueChange = {
                        baseAmount = it
                    },
                    keyboardOptions = KeyboardOptions(
                        keyboardType = KeyboardType.Number,
                        imeAction = ImeAction.Next
                    ),
                    modifier = Modifier
                        .fillMaxWidth(),
                    singleLine = true,
                    textStyle = LocalTextStyle.current.copy(color = Color.Black)
                )
            }

        }

        Spacer(modifier = Modifier.height(8.dp))

        Text(
            text = "Select Currency",
            modifier = Modifier.fillMaxWidth(),
            textAlign = TextAlign.Start
        )

        Row {

            Box(
                modifier = Modifier
                    .clickable {
                        expandedSelected = true
                    }
                    .border(1.dp, Color.Gray, shape = RoundedCornerShape(4.dp))
                    .background(Color.White)
                    .padding(horizontal = 8.dp, vertical = 8.dp)
            ) {
                Text(
                    text = selectedCurrency,
                    modifier = Modifier
                        .width(90.dp)
                        .height(20.dp),
                    textAlign = TextAlign.Center
                )
                Icon(
                    imageVector = Icons.Default.ArrowDropDown,
                    contentDescription = null,
                    modifier = Modifier.align(Alignment.CenterEnd)
                )
            }

            DropdownMenu(
                expanded = expandedSelected,
                onDismissRequest = {
                    expandedSelected = false
                }
            ) {
                selectedCurrencyCode.sorted().forEach { item ->
                    DropdownMenuItem(
                        onClick = {
                            selectedCurrency = item
                            expandedSelected = false
                        },
                        modifier = Modifier
                            .width(80.dp)
                    ) {
                        Text(text = item)
                    }
                }
            }
            Spacer(modifier = Modifier.width(5.dp))

            convertedAmount?.let {
                Box(
                    modifier = Modifier
                        .border(0.5.dp, Color.Gray, shape = RoundedCornerShape(4.dp))
                        .background(Color.White)
                        .padding(horizontal = 8.dp, vertical = 9.dp)
                ) {
                    BasicTextField(
                        readOnly = true,
                        value = it,
                        onValueChange = {
                            convertedAmount = it
                        },
                        keyboardOptions = KeyboardOptions(
                            keyboardType = KeyboardType.Number,
                            imeAction = ImeAction.Next
                        ),
                        modifier = Modifier
                            .fillMaxWidth(),
                        singleLine = true,
                        textStyle = LocalTextStyle.current.copy(color = Color.Black)
                    )
                }

            }
        }

        Spacer(modifier = Modifier.height(8.dp))

        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(24.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center,
        ) {
            Text(
                text = "Latest Rate",
                style = TextStyle(fontSize = 16.sp, fontWeight = FontWeight.Bold)
            )
            Text(
                text = "$baseAmount $baseCurrency = $convertedAmount $selectedCurrency",
                style = TextStyle(fontSize = 16.sp, fontWeight = FontWeight.Bold)
            )
        }

        Button(onClick = {
//            CoroutineScope(Dispatchers.IO).launch {
//                currencyPresenter.convertCurrency(
//                    from = baseCurrency,
//                    to = selectedCurrency,
//                    amount = baseAmount.toDouble()
//                )
//            }
            Toast.makeText(
                context,
                "Disable Convertion Function - API not subscribe",
                Toast.LENGTH_LONG
            ).show()
        }) {
            Text(text = stringResource(id = R.string.btn_convert))
        }
    }
}

//For MPChartLib
@Composable
fun LineChartComposable(labels: List<String>, entries: List<Entry>) {
    val context = LocalContext.current
    Utils.init(context)

    val data = remember { generateData(labels = labels, entries = entries) }
    val modifier = Modifier
        .fillMaxWidth()

    Box(
        modifier = modifier,
        contentAlignment = Alignment.Center
    ) {
        println("Label Size: ${labels.size}")
        println("Label Size: ${labels}")
        AndroidView(
            factory = { context ->
                val chart = LineChart(context)
                chart.description.isEnabled = false
                chart.isDragEnabled = true
                chart.setScaleEnabled(true)
                chart.setPinchZoom(true)
                chart.setDrawGridBackground(false)

                // X-axis configuration
                val xAxis = chart.xAxis
                xAxis.position = XAxis.XAxisPosition.BOTTOM
                xAxis.setDrawGridLines(false)
                xAxis.setLabelCount(labels.size, true)
                xAxis.setAvoidFirstLastClipping(false)
                xAxis.valueFormatter = IndexAxisValueFormatter(labels)

                // Y-axis configuration
                val yAxisLeft = chart.axisLeft
                yAxisLeft.setDrawGridLines(false)

                val yAxisRight = chart.axisRight
                yAxisRight.isEnabled = false

                chart.animateX(1500)

                chart
            },
            update = { chart ->
                chart.data = data.lineData
                chart.invalidate()
            },
            modifier = modifier
                .fillMaxSize()
                .padding(bottom = 10.dp)

        )
    }
}

data class ChartData(val labels: List<String>, val lineData: LineData)

fun generateData(labels: List<String>, entries: List<Entry>): ChartData {
    val dataSet = LineDataSet(entries, "Currency\nExchange Rates")
    dataSet.colors = ColorTemplate.MATERIAL_COLORS.toList()
    dataSet.valueTextColor = ColorTemplate.COLOR_NONE
    dataSet.circleColors = dataSet.colors

    val lineData = LineData(dataSet)
    return ChartData(labels, lineData)
}

// FOR charts-kt.io
data class PopCount(val year: Int, val population: Double)

@Composable
fun CurrencyChartView(populationData: List<PopCount>) {
    AndroidView(
        factory = { context ->
            CurrencyChart(context, populationData)
        },
        modifier = Modifier
            .fillMaxSize()
            .padding(bottom = 10.dp)
    )
}