package com.example.currencyexchange.presenter

import android.app.Application
import com.example.currencyexchange.CurrencyDataModel
import com.example.currencyexchange.data.contract.CurrencyContract
import com.example.currencyexchange.data.model.CurrencyModel
import com.example.currencyexchange.data.room.CurrencyDao
import com.example.currencyexchange.data.room.RoomCurrencyModel

class CurrencyPresenter(
    currencyView: CurrencyContract.View,
    application: Application, currencyDao: CurrencyDao
) :
    CurrencyContract.Presenter, CurrencyContract.Model.OnFinishedListener {
    private var currencyData: CurrencyContract.Model = CurrencyDataModel(
        application = application, currencyDao = currencyDao
    )

    private var currencyV: CurrencyContract.View? = null

    init {
        this.currencyV = currencyView
    }

    override fun onFinished(currencyModel: CurrencyModel) {
        currencyV?.setData(currencyModel)
        currencyV?.hideProgress()
    }

    override fun onFinished(roomCurrencyModel: RoomCurrencyModel) {
        currencyV?.setLocalData(roomCurrencyModel)
        currencyV?.hideProgress()
    }

    override fun onFailure(t: Throwable) {
        currencyV?.onResponseFailure(t)
        currencyV?.hideProgress()
    }

    override fun onDestroy() {
        this.currencyV = null
    }

    override fun requestData(base: String) {
        currencyV?.showProgress()
        currencyData.getExchangeRates(base = base, this)
    }

    override fun convertCurrency(from: String, to: String, amount: Double) {
        currencyV?.showProgress()
        currencyData.convertRates(from = from, to = to, amount = amount, this)

    }
}