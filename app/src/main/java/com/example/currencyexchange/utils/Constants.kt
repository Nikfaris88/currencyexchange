package com.example.currencyexchange.utils

class Constants {

    companion object {
        const val API_KEY = "f128e039a87a65b9908b9b9cb2fe8724"
        const val BASE_URL = "http://api.exchangeratesapi.io"
        const val CURRENCY_ENDPOINT = "v1/latest?"
        const val CONVERT_ENDPOINT = "v1/convert?"
    }
}