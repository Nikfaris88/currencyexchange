package com.example.currencyexchange

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.os.Build
import android.util.Log
import androidx.room.Room
import com.example.currencyexchange.data.api.ApiModule
import com.example.currencyexchange.data.api.ApiService
import com.example.currencyexchange.data.contract.CurrencyContract
import com.example.currencyexchange.data.model.CurrencyConvertResponse
import com.example.currencyexchange.data.model.CurrencyModel
import com.example.currencyexchange.data.room.CurrencyDao
import com.example.currencyexchange.data.room.CurrencyDatabase
import com.example.currencyexchange.data.room.RoomCurrencyModel
import com.example.currencyexchange.utils.Constants
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CurrencyDataModel(
    application: Application,
    private val currencyDao: CurrencyDao
) : CurrencyContract.Model {

    private lateinit var currencyModel: CurrencyModel
    private lateinit var currencyConvertResponse: CurrencyConvertResponse
    private val apiService = ApiModule().getClient().create(ApiService::class.java)
    private var database: CurrencyDatabase = Room.databaseBuilder(
        application,
        CurrencyDatabase::class.java,
        "currency_model"
    ).build()

    private val connectivityManager =
        application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    private val _isNetworkConnected = MutableStateFlow(false)
    private val isNetworkConnected: StateFlow<Boolean> = _isNetworkConnected

    init {

        registerNetworkCallback()
    }

    override fun getExchangeRates(
        base: String,
        onFinishedListener: CurrencyContract.Model.OnFinishedListener
    ) {
        if (isNetworkConnected.value) {
            CoroutineScope(Dispatchers.IO).launch {
                val result = apiService.getExchangeRates(accessKey = Constants.API_KEY, base = base)
                withContext(Dispatchers.Main) {
                    if (result.success) {
                        Log.d("TAG", "DATA:: $result")
                        currencyModel = result
                        val ratesJson = Gson().toJson(result.rates)
                        val currencyData = RoomCurrencyModel(
                            base = result.base,
                            date = result.date,
                            rates = ratesJson,
                            success = result.success,
                            timestamp = result.timestamp
                        )
                        withContext(Dispatchers.IO) {
                            database.currencyDao().insertNewExchangeRate(currencyData)
                        }
                        onFinishedListener.onFinished(result)
                    } else {
                        Log.e("TAG", "ERROR:: $result")
                        onFinishedListener.onFailure(t = Throwable("ERROR Fetching Data"))
                    }
                }
            }
        } else {
            val localData = currencyDao.getExchangeRateByBaseCurrency(base = base)
            if (localData != null) {
                Log.d("TAG", "LOCAL DATA:: $localData")
                onFinishedListener.onFinished(localData)
            } else {
                Log.e("TAG", "ERROR:: $localData")
                onFinishedListener.onFailure(t = Throwable("No data available"))
            }
        }

    }

    override fun convertRates(
        from: String,
        to: String,
        amount: Double,
        onFinishedListener: CurrencyContract.Model.OnFinishedListener
    ) {
        if (isNetworkConnected.value) {
            CoroutineScope(Dispatchers.IO).launch {
                val response = apiService.convertRates(
                    accessKey = Constants.API_KEY,
                    from = from,
                    to = to,
                    amount = amount
                )

                withContext(Dispatchers.Main) {
                    if (response.success) {
                        Log.d("TAG", "DATA:: $response")

                        currencyConvertResponse = response
                        val currencyData = RoomCurrencyModel(
                            base = response.query.from,
                            date = response.date,
                            rates = "${response.query.from}:${response.result}",
                            success = true,
                            timestamp = response.info.timestamp
                        )
                        withContext(Dispatchers.IO) {
                            database.currencyDao().insertNewExchangeRate(currencyData)
                        }
                        onFinishedListener.onFinished(currencyData)
                    } else {
                        Log.e("TAG", "ERROR")
                        onFinishedListener.onFailure(t = Throwable("ERROR Fetching Data"))
                    }
                }
            }
        } else {
            val localData = currencyDao.getExchangeRateByBaseCurrency(base = from)
            if (localData != null) {
                Log.d("TAG", "LOCAL DATA:: $localData")
                onFinishedListener.onFinished(localData)
            } else {
                Log.e("TAG", "ERROR:: $localData")
                onFinishedListener.onFailure(t = Throwable("No data available"))
            }
        }
    }

    private fun registerNetworkCallback() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            connectivityManager.registerDefaultNetworkCallback(object :
                ConnectivityManager.NetworkCallback() {
                override fun onAvailable(network: Network) {
                    _isNetworkConnected.value = true
                }

                override fun onLost(network: Network) {
                    _isNetworkConnected.value = false
                }
            })
        }
    }

}