package com.example.currencyexchange.ui

import android.annotation.SuppressLint
import android.content.Context
import com.example.currencyexchange.PopCount
import io.data2viz.charts.chart.Chart
import io.data2viz.charts.chart.chart
import io.data2viz.charts.chart.discrete
import io.data2viz.charts.chart.mark.area
import io.data2viz.charts.chart.quantitative
import io.data2viz.geom.Size
import io.data2viz.viz.VizContainerView

@SuppressLint("ViewConstructor")
class CurrencyChart(context: Context, data: List<PopCount>) : VizContainerView(context) {

    private val dataSize = 400.0

    private val chart: Chart<PopCount> = chart(data) {
        //size to configure the width and height of graph
        size = Size(250.0, 250.0)
        title = "Currency Exchange Rates"

        // Create a discrete dimension for the year of the census
        val year = discrete({ domain.year })


        // Create a continuous numeric dimension for the population
        val population = quantitative({ domain.population }) {
            name = "Currency Value based on Country"
        }

        // Using a discrete dimension for the X-axis and a continuous one for the Y-axis
        area(year, population)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        chart.size = Size(dataSize, dataSize * h / w)
    }
}